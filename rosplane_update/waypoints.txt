// waypoints in local NED: North[m] East[m] Down[m] heading[deg] speed[m/s]
// Keep heading degrees i the interval -180 < heading <= 180 to get the best result from plotting
// heading degrees measured around the unit circle where 0 degrees points north and 
// increases traversing counter-clockwise as shown below in the HEADING COMPASS.
//
//             HEADING COMPASS
//
//     -----------------------------
//     |             N             |
//     |                           |  
//     |       45    0    -45      |
//     |          \  |  /          |
//     |            \|/            |
//     | W    90 ----+---- -90   E |
//     |            /|\            |
//     |          /  |  \          |
//     |      135   180  -135      |
//     |                           |
//     |             S             |
//     -----------------------------
//
//
// Each waypoint should be on a separate line
//
//  ADD WAYPOINTS HERE

90 -7 -10 -45 12
90 53 -10 -135 12
26 43 -10 90 12
0 0 -10 180 12

//  END WAYPOINTS
