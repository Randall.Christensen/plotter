#!/usr/bin/env python

#       __DUBINSAIRPLANEMAIN__
#       This is the main file to execute examples of the Dubins Airplane mode
#       that supports 16 cases of possible trajectories
#
#       Authors:
#       Kostas Alexis (konstantinos.alexis@mavt.ethz.ch)

#       Modified by:
#       Terran Gerratt (Original code: https://github.com/unr-arl/DubinsAirplane)

from DubinsAirplaneFunctions import *
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import numpy as np
import time
import sys


def error_code(e):
    if e == 0:
        print("Error: Conservative condition (end_node[0:2] - start_node[0:2],ord=2) < 6*R_min) not fulfilled!\n"
              "       Start and end pose are close together. Path of type RLR, LRL may be optimal\n"
              "       May fail to compute optimal path! Aborting\n")
    sys.exit()


def plot_dubins(ax, waypoints, bank_max=pi / 4, gamma_max=pi / 6):
    # declare list of lists to keep mins and maxes
    corners = [[], [], [], []]
    for i in range(len(waypoints) - 1):
        R_min = MinTurnRadius_DubinsAirplane(waypoints[0][4], bank_max)

#        if (np.linalg.norm(waypoints[i + 1][0:2] - waypoints[i][0:2],ord=2) < 6*R_min):
#            print(i)
#            error_code(0)

        DubinsAirplaneSolution = DubinsAirplanePath(waypoints[i], waypoints[i + 1], R_min, gamma_max)
        path_dubins_airplane = ExtractDubinsAirplanePath(DubinsAirplaneSolution).T

        # list of mins and maxs of plots
        if ax.name == 'rectilinear':
            # plot dubins
            ax.plot(path_dubins_airplane[:, 0], path_dubins_airplane[:, 1], color='r')
            # plot waypoint
            ax.scatter(waypoints[i][0], waypoints[i][1], color='gold')
            # annotate waypoint number
            ax.text(waypoints[i][0], waypoints[i][1], '%s' % str(i), size=15, zorder=1, color='k')
            if i == len(waypoints) - 2:
                ax.scatter(waypoints[i + 1][0], waypoints[i + 1][1], color='gold')
                ax.text(waypoints[i + 1][0], waypoints[i + 1][1], '%s' % str(i + 1), size=15, zorder=1, color='k')
        else:
            # plot dubins
            ax.plot3D(path_dubins_airplane[:, 0], path_dubins_airplane[:, 1], path_dubins_airplane[:, 2], color='r')
            # plot waypoint
            ax.scatter3D(waypoints[i][0], waypoints[i][1], waypoints[i][2], color='gold')
            # annotate waypoint number
            ax.text(waypoints[i][0], waypoints[i][1], waypoints[i][2], '%s' % str(i), size=15, zorder=1, color='k')
            if i == len(waypoints) - 2:
                ax.scatter3D(waypoints[i + 1][0], waypoints[i + 1][1], waypoints[i + 1][2], color='gold')
                ax.text(waypoints[i + 1][0], waypoints[i + 1][1], waypoints[i + 1][2], '%s' % str(i + 1), size=15, zorder=1, color='k')

        corners[0].append(float(np.max(path_dubins_airplane[:, 0])))
        corners[1].append(float(np.min(path_dubins_airplane[:, 0])))
        corners[2].append(float(np.max(path_dubins_airplane[:, 1])))
        corners[3].append(float(np.min(path_dubins_airplane[:, 1])))

    # take the mins and maxes of all the line segments to create the absolute mins and maxs of the plot
    corners[0] = np.max(corners[0])  # max x
    corners[1] = np.min(corners[1])  # min x
    corners[2] = np.max(corners[2])  # max y
    corners[3] = np.min(corners[3])  # min y

    return ax, corners
